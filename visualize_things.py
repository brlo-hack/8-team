import csv
from collections import defaultdict, namedtuple

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd

# =======================================================================================
# data preparation stuff (from raw .csv)
DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
HOURS = np.concatenate((np.arange(5, 24), np.arange(5)))
HOURS = [str(h) for h in HOURS]
DayEntry = namedtuple("DayEntry", ["hour_array", "total", "day_of_week"])


def make_default_entry():
    return DayEntry(np.zeros(24, dtype="int32"), 0, "UNKNOWN")


def make_entry(hour_entries, total, day_of_week):
    def entry_to_int(entry):
        if entry == "":
            return 0
        else:
            return int(float(entry))  # thanks rene

    hour_array = np.array([entry_to_int(entry) for entry in hour_entries], dtype="int32")
    return DayEntry(hour_array, int(total), DAYS[int(day_of_week)])

# =======================================================================================
# further data processing (from dict)
def prepare_data_m(product_dict):
    def process(day_tuple):
        return day_tuple.hour_array.sum()  # total per day
    # averages over hours of the day
    averaged = dict([(key, process(val)) for key, val in product_dict.items()])
    # sort by date (string) and then only take values
    sorted_data = [pair[1] for pair in sorted(averaged.items())]
    return sorted_data


def prepare_data_d(product_dict):
    # NOTE x-labels are not "correct" in terms of hour of day!!
    # also: sum/31 instead of mean because
    data = np.sum([day_tuple.hour_array for day_tuple in product_dict.values()], axis=0) / 31
    return data


def prepare_data_r(product_dict):
    def process(day_tuple):
        return day_tuple.hour_array
    # just take arrays for each day
    array = dict([(key, process(val)) for key, val in product_dict.items()])
    # sort by date (string) and then only take values
    concat_data = np.concatenate([pair[1] for pair in sorted(array.items())])
    return concat_data


def plot(data, mode):
    df = pd.DataFrame({"sold": data})
    
    if mode == "m":
        df["dayofmonth"] = np.arange(1, len(data) + 1)
        ax = sns.pointplot(data=df, x="dayofmonth", y="sold")
        ax.set(xlabel="Day of month", ylabel="Number of items sold")
        
    if mode == "d":
        df["hour"] = np.arange(1, len(data) + 1)
        # start with hour 5, so offset 4
        df["hour"] = df["hour"] + 4
        df["hour"] = df["hour"] % 24
        ax = sns.pointplot(data=df, x="hour", y="sold")
        ax.set(xlabel="Hour of day", ylabel="Number of items sold")
    
    fig = plt.gcf()
    fig.set_size_inches(14, 10)
    plt.show()
        

def load_data_dict():
    # create a huge mess of a data structure
    data_dict = defaultdict(lambda: defaultdict(lambda: make_default_entry()))
    with open("data/data.csv") as data_csv:
        reader = csv.reader(data_csv)
        next(reader)  # skip header
        for row in reader:
            item_name = row[1]
            date = row[-2]
            data_dict[item_name][date] = make_entry(row[3:-2], row[2], row[-1])
    return data_dict

def show(mode, product_data):
    if mode == "m":
        d = prepare_data_m(product_data)
        plot(d, mode)
    elif mode == "d":
        d = prepare_data_d(product_data)
        plot(d, mode)
    elif mode == "r":
        d = prepare_data_r(product_data)
        plot(d, mode)


# days_sorted = sorted(list(data_dict["Gesamt"].keys()))

def main():
    data_dict = load_data_dict()
    while True:
        print("Name of the product of interest please:")
        product = input()
        product_data = data_dict[product]
        print("Show development over month (averaged over daytime): m")
        print("Show development over daytime (averaged over month): d")
        print("Show raw data over all hours of the month: r")
        mode = input()
        if mode == "m":
            d = prepare_data_m(product_data)
            plot(d)
        elif mode == "d":
            d = prepare_data_d(product_data)
            plot(d)
        elif mode == "r":
            d = prepare_data_r(product_data)
            plot(d)

if __name__ == "__main__":
    main()