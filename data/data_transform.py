import pandas as pd
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.expand_frame_repr', False)

df = pd.read_csv('data.csv')
# print(data.head())
dfstacked = df.stack(level=0)

times = df.columns[3:27]
print(list(times))

dfT = dfstacked.transpose()
# print(dfT)
# print(dfT.unstack(level=-1))

dfT.to_csv('transformed.csv')
# for row in dfT.iterrows():
#     if row[0] == 'date':
#         print(list(row[1]))
