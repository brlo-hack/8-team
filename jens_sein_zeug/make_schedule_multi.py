import csv
import subprocess

from matplotlib import pyplot as plt


def make_fact(request_row, lp_handle, ind, jobs_list):
    brewery, bottle, label, time_needed = request_row
    lp_handle.write("request(" + brewery + ", " + bottle + ", " + label + ", " + time_needed + ", " + str(ind) + ").\n")
    jobs_list.append(brewery + "-" + bottle + "-" + label)


def make_table(alloc_strings):
    table = [["" for _ in range(N_JOBS)] for _ in range(TIME_LIM)]
    colors = [["white" for _ in range(N_JOBS)] for _ in range(TIME_LIM)]
    for alloc in alloc_strings:
        numbers = [int(num) for num in alloc[10:-1].split(",")]
        id, phase, line, start, finish = numbers
        for eeeh in range(start, finish):
            if table[eeeh][line-1] != "":
                table[eeeh][line-1] += "//"
            table[eeeh][line-1] += jobs_by_ids[id] + "-P" + str(phase)
            if colors[eeeh][line-1] == "white":  # new (single) entry
                colors[eeeh][line-1] = PHASE_COLORS[phase]
            elif colors[eeeh][line-1] == "orange":  # triple entry
                colors[eeeh][line-1] = "purple"
            else:  # double entry
                colors[eeeh][line-1] = "orange"
    return table, colors


def make_hours():
    out = []
    for ind in range(TIME_LIM):
        out.append(str(ind).zfill(2) + ":00 - " + str(ind+1).zfill(2) + ":00")
    return out

if __name__ == "__main__":
    # create the logic program
    TIME_LIM = 60
    N_JOBS = 2
    PHASE_COSTS = [2, 3, 1]
    PHASE_COLORS = ["blue", "green", "red"]
    jobs_by_ids = []
    with open("requests_test_multi.lp", mode="w") as log_prog:
        log_prog.write("timelimit(" + str(TIME_LIM) + ").\n")
        log_prog.write("linelimit(" + str(N_JOBS) + ").\n")
        for phase, cost in enumerate(PHASE_COSTS):
            log_prog.write("phase(" + str(phase) + ", " + str(cost) + ").\n")
        log_prog.write("\n\n")
        with open("requests_test_bigger.csv", mode="r") as csv_file:
            reader = csv.reader(csv_file)
            next(reader)  # skip header
            for ind, row in enumerate(reader):
                make_fact(row, log_prog, ind, jobs_by_ids)
        log_prog.write("\n\n")
        with open("schedule_base_multi.lp", mode="r") as base_lp:
            for line in base_lp:
                log_prog.write(line)

    # solve it
    grounded = subprocess.run(("./gringo.exe", "requests_test_multi.lp"), stdout=subprocess.PIPE)
    assignments = subprocess.run(("./clasp.exe",), input=grounded.stdout, stdout=subprocess.PIPE)
    out_raw = str(assignments.stdout).split(r"\r\n")

    # extract stuff and make table
    some_ind = out_raw.index("OPTIMUM FOUND")
    opt_val = int(out_raw[some_ind - 1].split()[-1])
    TIME_LIM = opt_val
    opt_alloc = out_raw[some_ind - 2]
    table, colors = make_table(opt_alloc.split())

    # some fucking plot bullshit fuck
    fig, ax = plt.subplots(figsize=(15, 8), frameon=False)
    # Hide axes
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.table(cellText=table,
             cellColours=colors,
             rowLabels=make_hours(),
             colLabels=["Line " + str(ind) for ind in range(1, N_JOBS + 1)],
             loc="center")
    plt.show()
